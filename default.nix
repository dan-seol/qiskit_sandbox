{ pkgs ? import <nixpkgs> {}}:
let
  fhs = pkgs.buildFHSUserEnv {
    name = "qiskit_sandbox";

    targetPkgs = _: [
      pkgs.micromamba
    ];
    profile = ''
      set -e
      eval "$(micromamba shell hook --shell=posix)"
      micromamba activate qiskit_sandbox
      set +e
    '';
  };
in fhs.env
